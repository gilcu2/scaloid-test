package scaloid.example

import org.junit.Assert._
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import org.robolectric.{Robolectric, RobolectricTestRunner}

@RunWith(classOf[RobolectricTestRunner])
@Config(manifest = "AndroidManifest.xml")
class HelloScaloidTest {
  @Test def testButtonPressed(): Unit = {
    val activity = Robolectric.setupActivity(classOf[HelloScaloid])
    assertTrue(activity.urlEdit.text == "Me too")
    activity.okBtn.performClick()
    assertTrue(activity.okBtn.text == "PRESSED")
  }
}