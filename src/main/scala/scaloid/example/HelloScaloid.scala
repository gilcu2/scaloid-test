package scaloid.example

import java.lang.Exception
import java.net.URL

import org.scaloid.common._
import android.graphics.Color
import scala.concurrent.Future
import scala.concurrent.ExecutionContext
import android.os.AsyncTask
import android.util.Log

import scala.util.{Failure, Success}

object logDebug {
  def apply(s:String)={
    Log.d("DEBUG",s)
  }
}

class HelloScaloid extends SActivity {
  lazy val pageSrcView = new STextView()
  lazy val urlEdit = new SEditText("http://api.androidhive.info/progressdialog/hive.jpg")
  lazy val okBtn = new SButton("Download")

  onCreate {
    logDebug("begin onCreate")
    contentView = new SVerticalLayout {

      style {
        case b: SButton => b.textColor(Color.RED)
        case t: STextView => t textSize 10.dip
        case e: SEditText => e.backgroundColor(Color.YELLOW).textColor(Color.BLACK)
      }

      new SLinearLayout {
        STextView("URL: ")
        urlEdit.here
      }.wrap.here

      okBtn.here

      pageSrcView.here.fill
    } padding 20.dip


    okBtn.onClick({
      okBtn.text = "PRESSED"
      logDebug("begin onClick")
      val url=urlEdit.getText.toString
      val progressDialog = spinnerDialog("Scaloid Navigator",s"Download $url in progress...")

      implicit val exec = ExecutionContext.fromExecutor(
        AsyncTask.THREAD_POOL_EXECUTOR)

      Future {
        val lines = runJob(url)
        progressDialog.onComplete {
          case Success(v) => {
            logDebug(s"succes: $v")
            v.dismiss()
          }
          case Failure(e) => logDebug(s"fail: $e")
        }
        runOnUiThread(pageSrcView.setText(lines))
      }
    })
  }

  def runJob(uri: String): String = {
    logDebug("Traing to download " + uri)
    try {
      val url = new URL(uri)
      val conn = url.openConnection()
      conn.connect()
      val len = conn.getContentLength
      logDebug(s"len: $len")
      val input = conn.getInputStream
      val buf = new Array[Byte](1024)
      val count = input.read(buf, 0, 1024)
      val lines = buf.mkString("\n")
      logDebug("lines: " + lines)
      lines
    }
    catch {
      case x: Throwable => {
        val msg = s"exception $x"
        logDebug(msg)
        msg
      }
    }
  }


}
